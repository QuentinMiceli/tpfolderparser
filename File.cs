﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFolderParser
{
    class File
    {
        private string filePath;
        private double fileSize;
        private string extension;

        public static List<File> fileList = new List<File>();

        public string FilePath
        {
            get => filePath; set => filePath = value;
        }
        public double FileSize
        {
            get => fileSize; set => fileSize = value;
        }
        public string Extension
        {
            get => extension; set => extension = value;
        }

        public File(string filePath, double fileSize, string extension)
        {
            FilePath = filePath;
            FileSize = fileSize;
            Extension = extension;
        }
        public static void ProcessFile(string path)
        {
            long size = new System.IO.FileInfo(path).Length;
            string extension = Path.GetExtension(path);

            File f = new File(path, size, extension);

            Console.WriteLine("File : {0} | {1} | {2}", f.FilePath, f.FileSize, f.Extension);

            fileList.Add(f);
        }

        public static void Afficher()
        {
            foreach (File f in fileList)
            {
                Console.WriteLine(f.FilePath);
            }
        }

    }
}
