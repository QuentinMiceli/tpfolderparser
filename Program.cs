﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFolderParser
{
    class Program
    {
        public static void Main(string[] args)
        {
            string path = "C:\\Users\\utilisateur\\Documents\\Cours AJC";
            if (System.IO.File.Exists(path))
            {
                File.Afficher();
            }
            else if (Directory.Exists(path))
            {
                Folder.ProcessFolder(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or folder", path);
            }

        }
    }
}
