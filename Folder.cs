﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFolderParser
{
    class Folder
    {
        private string folderName;
        private int nbSubfolders;
        private int nbFiles;

        public string FolderName
        {
            get => folderName; set => folderName = value;
        }
        public int NbSubfolders
        {
            get => nbSubfolders; set => nbSubfolders = value;
        }
        public int NbFiles
        {
            get => nbFiles; set => nbFiles = value;
        }

        public Folder(string folderName, int nbSubfolders, int nbFiles)
        {
            FolderName = folderName;
            NbSubfolders = nbSubfolders;
            NbFiles = nbFiles;
        }

        public static void ProcessFolder(string path)
        {
            Folder root = new Folder(path, 0, 0);
            Console.WriteLine("Folder : {0}", root.FolderName);

            List<string> files = new List<string>(Directory.GetFiles(root.FolderName));
            foreach (string file in files)
            {
                File.ProcessFile(file);
            }

            List<string> subfolders = new List<string>(Directory.GetDirectories(path));
            foreach (string subfolder in subfolders)
            {
                Folder f = new Folder(subfolder, 0, 0);
                Folder.ProcessFolder(f.FolderName);
            }

        }
    }
}
